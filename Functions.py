import cv2 as cv
import numpy as np


def Make_Coordinates(image,line_parameters):
    slope,intercept = line_parameters
    y1 = image.shape[0]
    y2 = int(y1 * (0.5))  #Length of the printed lines on the image
    x1 = int((y1 - intercept)/slope)
    x2 = int((y2 - intercept)/slope)
    return np.array([x1, y1, x2, y2])

def Road_Space()-> None:
    return None


def avergage_Lines(image,lines,boolowska):
    left_fit = []
    right_fit = []
    for line in lines:
        x1,y1,x2,y2 = line.reshape(4)
        parameters = np.polyfit((x1,x2),(y1,y2),1)
        slope = parameters[0]
        intercept = parameters[1]
        if slope < 0:
            left_fit.append((slope,intercept))
        else:
            right_fit.append((slope,intercept))
    left_fit_average = np.average(left_fit,axis=0)
    left_line = Make_Coordinates(image,left_fit_average)
    if boolowska == True:
        right_fit_average = np.average(right_fit,axis=0)
        right_line = Make_Coordinates(image,right_fit_average)
        return np.array([left_line,right_line])
    return np.array([left_line])

#Isolating region of interest: 
def Mask(image,Coordinates_list,threshold_1,threshold_2):
    #Canny Edge detector in orderd to detect lines in object
    image = cv.Canny(image,threshold_1,threshold_2)
    tTriangle = np.array([Coordinates_list]) # Vertexes of the mask region
    tMask = np.zeros_like(image)    #Copying Image size and filling it in 0(logic 0)
    cv.fillPoly(tMask,tTriangle,255)#Filling with the collor(white) - logic 1
    tMasked_Image = cv.bitwise_and(image,tMask) #logic AND to 
    return tMasked_Image

def Display_lines(image,lines,Colour,Thickness):
    tLineImage = np.zeros_like(image)
    if lines is not None:
        for line in lines:
            x1,y1,x2,y2 = line.reshape(4)
            cv.line(tLineImage,(x1,y1),(x2,y2),Colour,Thickness)
    return tLineImage